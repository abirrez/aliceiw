import logo from './logo.svg';
import './App.css';
import {Button, Modal} from 'react-bootstrap';
import axios from 'axios';
import {useEffect, useState} from 'react';
import {FormControl, Form} from 'react-bootstrap';
import {DataGrid, GridRowsProp, GridColDef} from '@mui/x-data-grid';
import {styled, Box} from '@mui/system';
import ModalUnstyled from '@mui/base/ModalUnstyled';

const StyledModal = styled(ModalUnstyled)`
  position: fixed;
  z-index: 1300;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: rgba(0, 0, 0, 0.5);
`;

const Backdrop = styled('div')`
  z-index: -1;
  position: fixed;
  right: 0;
  bottom: 0;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.5);
  -webkit-tap-highlight-color: transparent;
`;

const style = {
    width: "40vw",
    height: "40vh",
    bgcolor: 'white',
    border: '2px solid #000',
    p: 2,
    px: 4,
    pb: 3,
    overflow: "scroll",
};


function App() {

    const [pageSize, setPageSize] = useState(5);
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([])
    const [name, setName] = useState("")
    const [search, setSearch] = useState("")
    const [show, setShow] = useState(false);
    const [selection, setSelection] = useState(null)
    const handleClose = () => setShow(false);

    const fetchData = async () => {
        setLoading(true);
        try {
            const {data: response} = await axios.get('https://entreprise.data.gouv.fr/api/sirene/v1/full_text/' + name + '?per_page=5&page=1');
            setData(response);
        } catch (error) {
            console.error(error.message);
            setData(null)
        }
        setLoading(false);
    }

    useEffect(() => {
        fetchData();
    }, [name])


    const columns = [
        {field: 'siren', headerName: 'Siren', width: 150},
        {field: 'l1_normalisee', headerName: 'Name', width: 600},

    ];
    return (
        <div className="App" style={{marginTop: "10vh"}}>
            <Form className="d-flex">
                <FormControl
                    style={{width: '30vw', height: '3vh', borderRadius: ' 5px'}}
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    onChange={(e) => {
                        setSearch(e.target.value)
                    }}
                />
                <Button style={{background: '#fcaf3b', marginLeft: '0.5vw', height: '3.1vh', borderRadius: ' 5px'}}
                        variant="outline-success" onClick={() => {
                    setName(search)
                }}>Search</Button>
            </Form>
            <div style={{height: '40vh', width: '40vw', margin: "5vh 30vw 30vh 30vw"}}>
                <DataGrid
                    style={{background:'whitesmoke'}}
                    pageSize={pageSize}
                    onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
                    rowsPerPageOptions={[5, 10, 15]}
                    pagination
                    rows={data ? data.etablissement : []} columns={columns} onRowClick={(e) => {
                    setSelection(data.etablissement.filter((s) => {
                        return e.row.id === s.id
                    }))
                    setShow(true)
                }}/>
            </div>

            <StyledModal
                aria-labelledby="unstyled-modal-title"
                aria-describedby="unstyled-modal-description"
                open={show}
                onClose={handleClose}
            >
                <Box sx={style}>
                    {selection ? JSON.stringify(selection, null, 2) : null}
                </Box>
            </StyledModal>


        </div>
    );
}

export default App;
